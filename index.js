let getCube = (num) => num ** 3;
let num = 2;
console.log(`The cube of ${num} is ${getCube(num)}`);

let address = [258, 'Purok 1-B', 'Talahiron', 'Kibawe', 'Bukidnon', 1234];
let [houseNumber, street, barangay, municipality, province, zipCode] = address;
console.log(
  `I live at ${houseNumber} ${street}, ${barangay}, ${municipality}, ${province}, ${zipCode}`
);

let animal = {
  name: 'Jepoy',
  species: 'Askal',
  weight: 250,
  measurement: 'kg',
  age: 10,
};
let { name, species, weight, measurement, age } = animal;
console.log(
  `The animal's name is ${name}, it's a ${species} , it weighs ${weight} ${measurement} and it is ${age} years old.`
);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => console.log(num));

let reduceNumber = numbers.reduce((acc, curr) => acc + curr);
console.log(`The sum of all numbers in the array is: ${reduceNumber}`);

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let dog1 = new Dog('Jepoy', 3, 'Askal');
console.log(dog1);
